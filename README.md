# Helm Argocd

# Gitlab Demo Repo
```bash
https://gitlab.com/quickbooks2018/helm-argocd.git
https://gitlab.com/quickbooks2018/foo.git
https://gitlab.com/quickbooks2018/argocd-image-tag.git
```
### Jenkins Setup
```bash
helm repo add jenkins https://charts.jenkins.io
helm repo update
helm repo ls
helm search repo jenkins
helm search repo jenkins/jenkins --versions
helm show values jenkins/jenkins --version 4.3.20
mkdir -p jenkins/values
helm show values jenkins/jenkins --version 4.3.20 > jenkins/values/jenkins.yaml
helm upgrade --install jenkins --namespace jenkins --create-namespace jenkins/jenkins --version 4.3.20 -f jenkins/values/jenkins.yaml --wait


# Set up port forwarding to the Jenkins UI from Cloud Shell
kubectl -n jenkins port-forward svc/jenkins --address 0.0.0.0 8090:8080

# Jenkins Secrets

kubectl get secrets -n jenkins 

kubectl get secrets/jenkins -n jenkins -o yaml

jenkins-admin-password: b1NuY1NpWmdnR25ZemtuNWx5Mnl0NQ==
jenkins-admin-user: YWRtaW4=

echo -n 'b1NuY1NpWmdnR25ZemtuNWx5Mnl0NQ==' | base64 -d
oSncSiZggGnYzkn5ly2yt5
```

### Argocd Setup

- ArgoCD HA & Non-HA Setup
- https://github.com/argoproj/argo-cd/releases

### Non-HA
```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.7.4/manifests/install.yaml
```

### HA
```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.7.4/manifests/ha/install.yaml
```

- Argocd kubectl commmands
```bash
kubectl get secrets -n argocd
kubectl get secrets/argocd-initial-admin-secret -n argocd -o yaml
echo -n 'UUE1alRPcEZ4RlQ5T0dOWQ==' | base64 --decode
kubectl -n argocd get svc
kubectl -n argocd port-forward svc/argocd-server --address 0.0.0.0 8091:443
```
- Helm argocd chart

```bash
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
helm repo ls
helm search repo argocd
helm show values argo/argo-cd --version 3.35.4
mkdir -p argocd/values
helm show values argo/argo-cd --version 3.35.4 > argocd/values/argocd.yaml
helm install argocd -n argocd --create-namespace argo/argo-cd --version 3.35.4 -f argocd/values/argocd.yaml
```
- Helm Image Updater
- https://argocd-image-updater.readthedocs.io/en/stable/
- https://argocd-image-updater.readthedocs.io/en/stable/basics/update-strategies/
- https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/
- https://argo-cd.readthedocs.io/en/release-2.1/operator-manual/declarative-setup/
### Image pull secrets must exist in the same Kubernetes cluster where Argo CD Image Updater is running in (or has access to). It is currently not possible to fetch those secrets from other clusters
- Note: first create secrets than apply application
- helm Image updater custom "argocd/values/argocd-image-updater.yaml"
- Step-1 Create Access token for gitlab registry
- https://argocd-image-updater.readthedocs.io/en/stable/basics/authentication/
- pullsecret:<namespace>/<secret_name>
- secret:<namespace>/<secret_name>#<field_name>

- https://github.com/argoproj-labs/argocd-image-updater/issues?q=gitlab
- https://github.com/argoproj-labs/argocd-image-updater/issues/483


- Argocd Image Updater Credentials in namespace argocd to read application Gitlab private registry
```bash
kubectl --namespace argocd create secret generic foo-gitlab-credentials \
    --from-literal=username=foo-gitlab-credentials \
    --from-literal=password=glpat-aMagVXPC2L7kLAedCzs_ 
```

- Argocd Image Updater Credentials in namespace argocd to write to helm gitlab repo
```bash
kubectl --namespace argocd create secret generic helm-repo-creds \
    --from-literal=username=ARGOCD-IMAGE-UPDATER \
    --from-literal=password=glpat-CtFg9ziQAVtWk1YDYmNe
```

- Image Pull Credentials in namespace foo-staging
```bash
kubectl --namespace foo-staging create secret docker-registry foo-gitlab-credentials \
  --docker-server=registry.gitlab.com \
  --docker-username=foo-gitlab-credentials \
  --docker-password=glpat-aMagVXPC2L7kLAedCzs_
```

- https://github.com/argoproj-labs/argocd-image-updater/issues/186

- https://argocd-image-updater.readthedocs.io/en/stable/basics/update-strategies/

- Gitlab Deploy Keys to access a specific repo over ssh
- https://docs.gitlab.com/ee/user/project/deploy_keys/index.html
```bash
ssh-keygen -t ed25519 -C "info@cloudgeek.ca" -f ~/.ssh/id_ed25519_helm
```

- Private ssh key for gitlab repo
```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: foo
  namespace: argocd
  labels:
    argocd.argoproj.io/secret-type: repository
type: Opaque
stringData:
  url: "ssh://git@gitlab.com/quickbooks2018/foo.git"
  sshPrivateKey: |
    -----BEGIN OPENSSH PRIVATE KEY-----
    your private ssh key
    -----END OPENSSH PRIVATE KEY-----
  insecure: "false"
  enableLfs: "false"
```  

```bash
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
helm repo ls
helm search repo argocd
helm show values argo/argocd-image-updater --version 0.9.1
mkdir -p argocd/values
helm show values argo/argocd-image-updater --version 0.9.1 > argocd/values/argocd-image-updater.yaml
helm upgrade --install argocd-image-updater -n argocd argo/argocd-image-updater --version 0.9.1 -f argocd/values/argocd-image-updater.yaml --wait
```

- Repo Pull & Push Setup in ArgoCD with Access Toekn

```bash
https://gitlab.com/quickbooks2018/kaniko.git
jenkins
glpat-xVz4Xp-14M6dvMMdWYKx
```

- Jenkins Service Account Role Binding with application namespace default service account
```bash
kubectl create rolebinding jenkins-admin-binding --clusterrole=admin --serviceaccount=jenkins:default --namespace=hello
```

- kind Metrics Server
- https://gist.github.com/sanketsudake/a089e691286bf2189bfedf295222bd43
```bash
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.5.0/components.yaml

cat <<EOF > metric-server-patch.yaml
spec:
  template:
    spec:
      containers:
      - args:
        - --cert-dir=/tmp
        - --secure-port=443
        - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
        - --kubelet-use-node-status-port
        - --metric-resolution=15s
        - --kubelet-insecure-tls
        name: metrics-server
EOF

kubectl patch deployment metrics-server -n kube-system --patch "$(cat metric-server-patch.yaml)"


helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
helm repo update
helm upgrade --install --set args={--kubelet-insecure-tls} metrics-server metrics-server/metrics-server --namespace kube-system
```

- ArgoCD
```bash
https://github.com/argoproj/argo-helm/tree/main/charts/argo-cd

https://github.com/argoproj/argo-cd/releases
```

- application.yaml
```bash
https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/application.yaml

https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/
```


- Bitnami Sealed Secrets

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm search repo bitnami/sealed-secrets
helm show values bitnami/sealed-secrets --version 1.2.11
helm show values bitnami/sealed-secrets --version 1.2.11 > values/sealed-secrets.yaml
helm upgrade --install sealed-secrets --namespace sealed-secrets --create-namespace bitnami/sealed-secrets --version 1.2.11 --wait
```

- install kube-seal for linux

-  https://github.com/bitnami-labs/sealed-secrets/releases

```bash
curl -LO -# https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.20.2/kubeseal-0.20.2-linux-amd64.tar.gz
tar -xzvf kubeseal-0.20.2-linux-amd64.tar.gz
sudo install -m 755 kubeseal /usr/local/bin/kubeseal
```

- Encrypt a secret from a file
- https://github.com/bitnami-labs/sealed-secrets/issues/758

```bash
 --controller-name string           Name of sealed-secrets controller. (default "sealed-secrets-controller")
      --controller-namespace string      Namespace of sealed-secrets controller. (default "kube-system")
```



```bash
kubeseal --controller-name sealed-secrets --controller-namespace=sealed-secrets -o yaml -n sealed-secrets < argocd-repo-secrets.yaml > sealed-argocd-repo-secrets.yaml
```

- ArgoCD CLI Installation
```bash
curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
chmod +x /usr/local/bin/argocd
argocd version
```

- Argocd CLI Specific Version
```bash
export VERSION=<TAG>
curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$VERSION/argocd-linux-amd64
chmod +x /usr/local/bin/argocd
argocd version
```

- Argocd Login (Not do port forward )
- Argocd Port Forward
```bash
kubectl -n argocd port-forward svc/argocd-server --address 0.0.0.0 8091:443
```
```bash
argocd login localhost:8091 --insecure
```
- Argocd Add aws Eks
```bash
kubectl config get-contexts

CURRENT   NAME                                                            CLUSTER                                                         AUTHINFO
                                          NAMESPACE
          arn:aws:eks:us-east-1:831872632567:cluster/cloudgeeks-eks-dev   arn:aws:eks:us-east-1:831872632567:cluster/cloudgeeks-eks-dev   arn:aws:eks:us-east-1:
831872632567:cluster/cloudgeeks-eks-dev
*         kind-cloudgeeks                                                 kind-cloudgeeks                                                 kind-cloudgeeks

➜ argocd cluster add arn:aws:eks:us-east-1:831872632567:cluster/cloudgeeks-eks-dev --name cloudgeeks-eks-dev --kubeconfig ./config
```

- kubectl get context commands
```bash
kubectl config get-contexts

kubectl config get-clusters

kubectl config use-context kind-cloudgeeks

kubectl config use-context arn:aws:eks:us-east-1:831872632567:cluster/cloudgeeks-eks-dev
```
